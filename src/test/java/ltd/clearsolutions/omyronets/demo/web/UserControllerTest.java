package ltd.clearsolutions.omyronets.demo.web;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;
import java.util.NavigableSet;
import ltd.clearsolutions.omyronets.demo.domain.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest
@AutoConfigureMockMvc
class UserControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private NavigableSet<User> users;

    @Value("${app.minage}")
    private int minAge;

    @BeforeEach
    public void setup() {
        this.users.clear();
        this.users.add(new User("Florizel",
                                "Brante",
                                "fl@gmail.com",
                                LocalDate.of(1953, 3, 17),
                                "Gerzen street",
                                "+1234556789"));
    }

    @Test
    void postValidUser_thenCreated() throws Exception {
        String requestBodyJson = """
            {
                "firstName": "John",
                "lastName": "Kramer",
                "email": "johncramer@gmail.com",
                "birthDate": "1962-01-12",
                "address": "10 Cherry Tree Lane",
                "phoneNumber": "+12345678910"
                        
            }""";
        String responseBodyJson = requestBodyJson;

        this.mockMvc.perform(post("/api/v1/users").content(requestBodyJson).contentType(APPLICATION_JSON))
                    .andExpect(status().isCreated())
                    .andExpect(content().contentType(APPLICATION_JSON))
                    .andExpect(content().json(responseBodyJson));

    }

    @Test
    void postMissingFieldsUser_thenBadRequestWithErrors() throws Exception {
        String requestBodyJson = """
            {
                       
            }""";
        String responseBodyJson = """
            {
                "status": 400,
                "error": "Bad Request",
                "message": "Validation error",
                "path": "/api/v1/users",
                "validationErrors": {
                    "lastName": "Last name cannot be blank",
                    "firstName": "First name cannot be blank",
                    "birthDate": "Birth date cannot be blank",
                    "email": "Email cannot be blank"
                }
            }""";

        this.mockMvc.perform(post("/api/v1/users").content(requestBodyJson).contentType(APPLICATION_JSON))
                    .andExpect(status().isBadRequest())
                    .andExpect(content().contentType(APPLICATION_JSON))
                    .andExpect(content().json(responseBodyJson));

    }

    @Test
    void postInvalidFieldsUser_thenBadRequestWithErrors() throws Exception {
        String requestBodyJson = """
            {
                "firstName": "John",
                "lastName": "Kramer",
                "email": "johncramergmailcom",
                "birthDate": "1962-01-12",
                "address": "10 Cherry Tree Lane",
                "phoneNumber": "sdf345678910"
            }""";
        String responseBodyJson = """
            {
                "status": 400,
                "error": "Bad Request",
                "message": "Validation error",
                "path": "/api/v1/users",
                "validationErrors": {
                    "phoneNumber": "Phone number should consist of digits and may start with ”+” sign",
                    "email": "must be a well-formed email address"
                }
            }""";

        this.mockMvc.perform(post("/api/v1/users").content(requestBodyJson).contentType(APPLICATION_JSON))
                    .andExpect(status().isBadRequest())
                    .andExpect(content().contentType(APPLICATION_JSON))
                    .andExpect(content().json(responseBodyJson));

    }

    @Test
    void postUnderAgeUser_thenBadRequestWithErrors() throws Exception {
        String requestBodyJson = """
            {
                "firstName": "John",
                "lastName": "Kramer",
                "email": "johncramer@gmail.com",
                "birthDate": "2020-01-12",
                "address": "10 Cherry Tree Lane",
                "phoneNumber": "+12345678910"
            }""";
        String responseBodyJson = """
            {
                "status": 400,
                "error": "Bad Request",
                "message": "Validation error",
                "path": "/api/v1/users",
                "validationErrors": {
                    "birthDate": "Must be at least AGE years old"
                }
            }""".replaceFirst("AGE", String.valueOf(minAge));

        this.mockMvc.perform(post("/api/v1/users").content(requestBodyJson).contentType(APPLICATION_JSON))
                    .andExpect(status().isBadRequest())
                    .andExpect(content().contentType(APPLICATION_JSON))
                    .andExpect(content().json(responseBodyJson));

    }

    @Test
    void postExistingUser_thenConflict() throws Exception {
        String requestBodyJson = """
            {
                "firstName": "Florizel",
                "lastName": "Kramer",
                "email": "johncramer@gmail.com",
                "birthDate": "1962-01-12",
                "address": "10 Cherry Tree Lane",
                "phoneNumber": "+12345678910"
            }""";
        String responseBodyJson = """
            {
                "status": 409,
                "error": "Conflict",
                "message": "User Florizel already exists",
                "path": "/api/v1/users"
            }""";

        this.mockMvc.perform(post("/api/v1/users").content(requestBodyJson).contentType(APPLICATION_JSON))
                    .andExpect(status().isConflict())
                    .andExpect(content().contentType(APPLICATION_JSON))
                    .andExpect(content().json(responseBodyJson));

    }


    @Test
    void getUsersByBornDateRange_thenOk() throws Exception {
        String responseBodyJson = """
            [
                {
                    "firstName": "Florizel",
                    "lastName": "Brante",
                    "email": "fl@gmail.com",
                    "birthDate": "1953-03-17",
                    "address": "Gerzen street",
                    "phoneNumber": "+1234556789"
                        
                }
            ]""";

        this.mockMvc.perform(get("/api/v1/users?fromDate=1951-01-01&toDate=2000-01-01"))
                    .andExpect(status().isOk())
                    .andExpect(content().contentType(APPLICATION_JSON))
                    .andExpect(content().json(responseBodyJson));

    }

    @Test
    void getUsersInvalidDateRange_thenBadRequest() throws Exception {
        String responseBodyJson = """
            {
                "status": 400,
                "error": "Bad Request",
                "message": "Validation error",
                "path": "/api/v1/users",
                "validationErrors": {
                    "filterParams": "Invalid date range"
                }
            }""";

        this.mockMvc.perform(get("/api/v1/users?fromDate=2000-01-01&toDate=1985-01-01"))
                    .andExpect(status().isBadRequest())
                    .andExpect(content().contentType(APPLICATION_JSON))
                    .andExpect(content().json(responseBodyJson));

    }

    @Test
    void getUser_thenOk() throws Exception {
        String responseBodyJson = """
            {
                    "firstName": "Florizel",
                    "lastName": "Brante",
                    "email": "fl@gmail.com",
                    "birthDate": "1953-03-17",
                    "address": "Gerzen street",
                    "phoneNumber": "+1234556789"
                        
            }""";

        this.mockMvc.perform(get("/api/v1/users/Florizel"))
                    .andExpect(status().isOk())
                    .andExpect(content().contentType(APPLICATION_JSON))
                    .andExpect(content().json(responseBodyJson));
    }

    @Test
    void getNonExistingUser_thenNotFound() throws Exception {
        String responseBodyJson = """
            {
                "status": 404,
                "error": "Not Found",
                "message": "User Brante not found",
                "path": "/api/v1/users/Brante"
            }""";

        this.mockMvc.perform(get("/api/v1/users/Brante"))
                    .andExpect(status().isNotFound())
                    .andExpect(content().contentType(APPLICATION_JSON))
                    .andExpect(content().json(responseBodyJson));
    }

    @Test
    void putUser_thenOk() throws Exception {
        String requestBodyJson = """
            {
                "firstName": "Brante",
                "lastName": "Florizel",
                "email": "br@gmail.com",
                "birthDate": "1967-03-17",
                "address": "Cherry street",
                "phoneNumber": "+987654321"
            }
            """;
        String responseBodyJson = requestBodyJson;

        this.mockMvc.perform(put("/api/v1/users/Florizel").content(requestBodyJson).contentType(APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andExpect(content().contentType(APPLICATION_JSON))
                    .andExpect(content().json(responseBodyJson));
    }

    @Test
    void putInvalidUser_thenBadRequest() throws Exception {
        String requestBodyJson = """
            {
                "email": "brgmail.com",
                "address": "Cherry street",
                "phoneNumber": "+98765sdf4321"
            }
            """;
        String responseBodyJson = """
            {
                "status": 400,
                "error": "Bad Request",
                "message": "Validation error",
                "path": "/api/v1/users/Florizel",
                "validationErrors": {
                    "lastName": "Last name cannot be blank",
                    "firstName": "First name cannot be blank",
                    "phoneNumber": "Phone number should consist of digits and may start with ”+” sign",
                    "birthDate": "Birth date cannot be blank",
                    "email": "must be a well-formed email address"
                }
            }
            """;

        this.mockMvc.perform(put("/api/v1/users/Florizel").content(requestBodyJson).contentType(APPLICATION_JSON))
                    .andExpect(status().isBadRequest())
                    .andExpect(content().contentType(APPLICATION_JSON))
                    .andExpect(content().json(responseBodyJson));
    }

    @Test
    void putNonExistingUser_thenNotFound() throws Exception {
        String requestBodyJson = """
            {
                "firstName": "Brante",
                "lastName": "Florizel",
                "email": "br@gmail.com",
                "birthDate": "1967-03-17",
                "address": "Cherry street",
                "phoneNumber": "+987654321"
            }
            """;
        String responseBodyJson = """
            {
                "status": 404,
                "error": "Not Found",
                "message": "User Brante not found",
                "path": "/api/v1/users/Brante"
            }""";

        this.mockMvc.perform(put("/api/v1/users/Brante").content(requestBodyJson).contentType(APPLICATION_JSON))
                    .andExpect(status().isNotFound())
                    .andExpect(content().contentType(APPLICATION_JSON))
                    .andExpect(content().json(responseBodyJson));
    }

    @Test
    void patchUser_thenOk() throws Exception {
        String requestBodyJson = """
            {
                "email": "br@gmail.com",
                "address": "Cherry street",
                "phoneNumber": "+987654321"
            }
            """;
        String responseBodyJson = """
            {
                "firstName": "Florizel",
                "lastName": "Brante",
                "email": "br@gmail.com",
                "birthDate": "1953-03-17",
                "address": "Cherry street",
                "phoneNumber": "+987654321"
            }""";

        this.mockMvc.perform(patch("/api/v1/users/Florizel").content(requestBodyJson).contentType(APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andExpect(content().contentType(APPLICATION_JSON))
                    .andExpect(content().json(responseBodyJson));

    }

    @Test
    void patchNonExistingUser_thenNotFound() throws Exception {
        String requestBodyJson = """
            {
                "email": "br@gmail.com",
                "address": "Cherry street",
                "phoneNumber": "+987654321"
            }
            """;
        String responseBodyJson = """
            {
                "status": 404,
                "error": "Not Found",
                "message": "User Brante not found",
                "path": "/api/v1/users/Brante"
            }""";

        this.mockMvc.perform(patch("/api/v1/users/Brante").content(requestBodyJson).contentType(APPLICATION_JSON))
                    .andExpect(status().isNotFound())
                    .andExpect(content().contentType(APPLICATION_JSON))
                    .andExpect(content().json(responseBodyJson));

    }

    @Test
    void patchIsInvalid_thenBadRequest() throws Exception {
        String requestBodyJson = """
            {
                "email": "brgmail.com",
                "address": "Cherry street",
                "phoneNumber": "+9876asdads54321"
            }
            """;
        String responseBodyJson = """
            {
                "status": 400,
                "error": "Bad Request",
                "message": "Validation error",
                "path": "/api/v1/users/Florizel",
                "validationErrors": {
                    "phoneNumber": "Phone number should consist of digits and may start with ”+” sign",
                    "email": "must be a well-formed email address"
                }
            }""";

        this.mockMvc.perform(patch("/api/v1/users/Florizel").content(requestBodyJson).contentType(APPLICATION_JSON))
                    .andExpect(status().isBadRequest())
                    .andExpect(content().contentType(APPLICATION_JSON))
                    .andExpect(content().json(responseBodyJson));

    }

    @Test
    void deleteUser_thenNoContent() throws Exception {
        this.mockMvc.perform(delete("/api/v1/users/Florizel"))
                    .andExpect(status().isNoContent());
    }

    @Test
    void deleteNonExistingUser_thenNotFound() throws Exception {
        String responseBodyJson = """        
        {
            "status": 404,
            "error": "Not Found",
            "message": "User Brante not found",
            "path": "/api/v1/users/Brante"
        }""";

        this.mockMvc.perform(delete("/api/v1/users/Brante"))
                    .andExpect(status().isNotFound())
                    .andExpect(content().contentType(APPLICATION_JSON))
                    .andExpect(content().json(responseBodyJson));
    }
}