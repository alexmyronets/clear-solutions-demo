package ltd.clearsolutions.omyronets.demo;

import java.util.Collections;
import java.util.Comparator;
import java.util.NavigableSet;
import java.util.TreeSet;
import ltd.clearsolutions.omyronets.demo.domain.User;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class DemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }

    @Bean
    public NavigableSet<User> usersList() {
        return Collections.synchronizedNavigableSet(new TreeSet<>(Comparator.comparing(User::firstName)));
    }

}
