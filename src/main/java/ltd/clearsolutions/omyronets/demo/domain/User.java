package ltd.clearsolutions.omyronets.demo.domain;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Past;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.groups.Default;
import java.time.LocalDate;
import ltd.clearsolutions.omyronets.demo.validation.MinAge;
import ltd.clearsolutions.omyronets.demo.validation.PatchValidation;

public record User(@NotEmpty(message = "First name cannot be blank") String firstName,
                   @NotEmpty(message = "Last name cannot be blank") String lastName,
                   @NotEmpty(message = "Email cannot be blank") @Email(groups = {Default.class,
                       PatchValidation.class}) String email,
                   @NotNull(message = "Birth date cannot be blank") @Past(message = "Birth date cannot be in future", groups = {
                       Default.class, PatchValidation.class}) @MinAge(groups = {
                       Default.class, PatchValidation.class}) LocalDate birthDate, String address,
                   @Pattern(regexp = "^\\+?[0-9]+$", message = "Phone number should consist of digits and may start with ”+” sign", groups = {
                       Default.class, PatchValidation.class}) String phoneNumber) {

}
