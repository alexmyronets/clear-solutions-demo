package ltd.clearsolutions.omyronets.demo.validation;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import java.time.LocalDate;
import java.time.Period;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class MinAgeValidator implements ConstraintValidator<MinAge, LocalDate> {

    @Value("${app.minage}")
    private int minAge;

    @Override
    public boolean isValid(LocalDate birthDate, ConstraintValidatorContext context) {
        // null values are valid
        if (birthDate == null) {
            return true;
        }

        boolean isValid = Period.between(birthDate, LocalDate.now()).getYears() >= minAge;

        if (!isValid) {
            context.disableDefaultConstraintViolation();
            context.buildConstraintViolationWithTemplate("Must be at least " + minAge + " years old")
                   .addConstraintViolation();
        }

        return isValid;
    }
}
