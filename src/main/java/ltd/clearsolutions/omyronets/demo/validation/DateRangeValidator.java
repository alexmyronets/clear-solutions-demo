package ltd.clearsolutions.omyronets.demo.validation;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import ltd.clearsolutions.omyronets.demo.dto.FilterParams;

public class DateRangeValidator implements ConstraintValidator<DateRange, FilterParams> {

    @Override
    public void initialize(DateRange constraintAnnotation) {
    }

    @Override
    public boolean isValid(FilterParams filterParams, ConstraintValidatorContext context) {
        if (filterParams == null) {
            return true; // Null values will be handled by @NotNull annotation
        }

        return !filterParams.fromDate().isAfter(filterParams.toDate());
    }
}
