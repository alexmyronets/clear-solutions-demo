package ltd.clearsolutions.omyronets.demo.dto;

import jakarta.validation.constraints.AssertTrue;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.PastOrPresent;
import java.time.LocalDate;
import ltd.clearsolutions.omyronets.demo.validation.DateRange;


public record FilterParams(@NotNull @PastOrPresent LocalDate fromDate, @NotNull @PastOrPresent LocalDate toDate) {

    @AssertTrue(message = "Date range is not valid")
    private boolean isValidDateRange() {
        return fromDate.isBefore(toDate);
    }

}
