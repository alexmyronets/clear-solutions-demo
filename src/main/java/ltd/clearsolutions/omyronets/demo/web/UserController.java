package ltd.clearsolutions.omyronets.demo.web;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import jakarta.validation.Valid;
import java.util.List;
import java.util.NavigableSet;
import ltd.clearsolutions.omyronets.demo.domain.User;
import ltd.clearsolutions.omyronets.demo.dto.FilterParams;
import ltd.clearsolutions.omyronets.demo.exception.NoSuchUserException;
import ltd.clearsolutions.omyronets.demo.exception.UserAlreadyExistsException;
import ltd.clearsolutions.omyronets.demo.validation.PatchValidation;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/users")
@Validated
public class UserController {

    private final NavigableSet<User> users;

    public UserController(NavigableSet<User> users) {
        this.users = users;
    }

    @PostMapping(consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public User postUser(@Valid @RequestBody User user) {
        if (!users.add(user)) {
            throw new UserAlreadyExistsException("User " + user.firstName() + " already exists");
        }
        return user;
    }

    @GetMapping(produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public List<User> getUsers(@Valid FilterParams filterParams) {
        return users.stream()
                    .filter(u -> u.birthDate().isAfter(filterParams.fromDate())
                                 && u.birthDate().isBefore(filterParams.toDate()))
                    .toList();
    }

    @GetMapping(path = "/{userName}", produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public User getUser(@PathVariable String userName) {
        User userToFind = new User(userName, null, null, null, null, null);
        User foundUser = users.floor(userToFind);

        if (foundUser != null) {
            return foundUser;
        }
        throw new NoSuchUserException("User " + userName + " not found");
    }

    @PutMapping(path = "/{userName}", produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public User putUser(@Valid @RequestBody User user, @PathVariable String userName) {
        if (!users.removeIf(u -> u.firstName().equals(userName))) {
            throw new NoSuchUserException("User " + userName + " not found");
        }
        users.add(user);
        return user;
    }

    @PatchMapping(path = "/{userName}", produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public User patchUser(@Validated(PatchValidation.class) @RequestBody User patch, @PathVariable String userName) {
        User userToFind = new User(userName, null, null, null, null, null);
        User originalUser = users.floor(userToFind);

        if (originalUser != null) {
            User patchedUser = new User(
                patch.firstName() != null ? patch.firstName() : originalUser.firstName(),
                patch.lastName() != null ? patch.lastName() : originalUser.lastName(),
                patch.email() != null ? patch.email() : originalUser.email(),
                patch.birthDate() != null ? patch.birthDate() : originalUser.birthDate(),
                patch.address() != null ? patch.address() : originalUser.address(),
                patch.phoneNumber() != null ? patch.phoneNumber() : originalUser.phoneNumber());
            users.remove(originalUser);
            users.add(patchedUser);
            return patchedUser;
        }
        throw new NoSuchUserException("User " + userName + " not found");
    }

    @DeleteMapping("/{userName}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteUser(@PathVariable String userName) {
        if (!users.removeIf(u -> u.firstName().equals(userName))) {
            throw new NoSuchUserException("User " + userName + " not found");
        }
    }
}
