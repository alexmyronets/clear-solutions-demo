package ltd.clearsolutions.omyronets.demo.web;

import jakarta.validation.ConstraintViolationException;
import java.util.HashMap;
import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import ltd.clearsolutions.omyronets.demo.exception.NoSuchUserException;
import ltd.clearsolutions.omyronets.demo.exception.UserAlreadyExistsException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(
        MethodArgumentNotValidException e, HttpHeaders headers, HttpStatusCode status, WebRequest request) {
        Map<String, String> errors = new HashMap<>();
        ValidationErrorResponse validationErrorResponse = new ValidationErrorResponse();

        validationErrorResponse.setStatus(HttpStatus.BAD_REQUEST.value());
        validationErrorResponse.setError(HttpStatus.BAD_REQUEST.getReasonPhrase());
        validationErrorResponse.setMessage("Validation error");
        validationErrorResponse.setPath(request.getDescription(false).replaceFirst("uri=", ""));
        e.getBindingResult()
         .getAllErrors()
         .forEach(error -> {
             if (error instanceof FieldError fielderror) {
                 String fieldName = fielderror.getField();
                 String errorMessage = error.getDefaultMessage();
                 errors.put(fieldName, errorMessage);
             } else {
                 String objectName = error.getObjectName();
                 String errorMessage = error.getDefaultMessage();
                 errors.put(objectName, errorMessage);
             }

         });
        validationErrorResponse.setValidationErrors(errors);
        return handleExceptionInternal(e, validationErrorResponse,
                                       headers, HttpStatus.BAD_REQUEST, request);
    }

    @ExceptionHandler(value = UserAlreadyExistsException.class)
    protected ResponseEntity<Object> handleUserAlreadyExistsException(UserAlreadyExistsException e, WebRequest request) {
        ErrorResponse errorResponse = new ErrorResponse();

        errorResponse.setStatus(HttpStatus.CONFLICT.value());
        errorResponse.setError(HttpStatus.CONFLICT.getReasonPhrase());
        errorResponse.setMessage(e.getMessage());
        errorResponse.setPath(request.getDescription(false).replaceFirst("uri=", ""));

        return handleExceptionInternal(e, errorResponse, new HttpHeaders(), HttpStatus.CONFLICT, request);
    }

    @ExceptionHandler(value = NoSuchUserException.class)
    public ResponseEntity<Object> handleUserException(NoSuchUserException e, WebRequest request) {
        ErrorResponse errorResponse = new ErrorResponse();

        errorResponse.setStatus(HttpStatus.NOT_FOUND.value());
        errorResponse.setError(HttpStatus.NOT_FOUND.getReasonPhrase());
        errorResponse.setMessage(e.getMessage());
        errorResponse.setPath(request.getDescription(false).replaceFirst("uri=", ""));

        return handleExceptionInternal(e, errorResponse, new HttpHeaders(), HttpStatus.NOT_FOUND, request);
    }

    @Data
    @RequiredArgsConstructor
    @AllArgsConstructor
    protected static class ErrorResponse {

        private int status;
        private String error;
        private String message;
        private String path;
    }

    @EqualsAndHashCode(callSuper = true)
    @Data
    protected static class ValidationErrorResponse extends ErrorResponse {
        Map<String, String> validationErrors;
    }

}
